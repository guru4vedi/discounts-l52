<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Order;

class Coupon extends Model
{
    /**
     * Find a Coupon by code
     */
    public static function findByCode($code)
    {
        return self::where('code', $code)->first();
    }

    /**
     * Link to corresponding coupon referenced by coupon_type
     */
    public function coupon()
    {
        return $this->morphTo();
    }

    /**
     * Return the final discount value for a given order
     */
    public function discount(Order $order)
    {
        return $this->coupon->discount($order);
    }

    /**
     * Does it offer any price discount?
     */
    public function hasDiscount()
    {
        return $this->coupon->hasDiscount();
    }

    /**
     * Return the final value for a given amount
     */
    public function discountedPrice($price)
    {
        return $this->coupon->discountedPrice($price);
    }

    /**
     * Friendly Name of the coupon
     */
    public function screenName($noCode=true)
    {
        if($noCode){
            return $this->coupon->screenName();
        } else {
            return sprintf("%s %s", $this->code, $this->coupon->screenName());
        }
    }
}