<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'base', 'conversion',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];


    protected $primaryKey = 'code';
    public $incrementing = false;

    /**
     * Fetch currencies that are active
     * 
     * @return App\Currency
     */
    public static function active()
    {
        $currencies = static::whereActive(true);

        return $currencies;
    }

    /**
     * Fetch currencies by rank that are active
     * 
     * @return App\Currency
     */
    public static function activeByRank()
    {
        $currencies = static::whereActive(true)
                                ->orderBy('rank', 'asc');

        return $currencies;
    }
}