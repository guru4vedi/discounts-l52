<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Order;

class PercentOffCoupon extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

	/**
	 * Return the computed discount value
	 */
    public function discount(Order $order)
    {
    	return $order->subTotal() * ($this->percent_off / 100);
    }

    /**
     * Does it offer any price discount?
     */
    public function hasDiscount()
    {
        return true;
    }

    /**
     * Return the final value for a given amount
     */
    public function discountedPrice($price)
    {
        return  $price - ($price * ($this->percent_off / 100));
    }

    /**
     * Friendly Name of the coupon
     */
    public function screenName()
    {
        return sprintf(" %d PercentOff", $this->percent_off);
    }
}