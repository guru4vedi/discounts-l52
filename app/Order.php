<?php

namespace App;

use App\Coupon;
use App\ServicePlan;

/**
 * Non-Eloquent Model for a Virtual Order
 */
class Order
{
	private $plans;
	private $coupon;
	private $currency;

	/**
	 * 
	 */
	public function __construct(ServicePlan $plan, $currency='USD')
	{
		$this->plans = collect();
		$this->plans->push($plan);
		$this->currency = $currency;
	}

	/**
	 * Gross total of all products in the order
	 */
	public function subTotal()
	{
		return $this->plans->sum('price');
	}

	/**
	 * Returns the grand total of the order
	 */
	public function total()
	{
		return $this->subTotal() - $this->discount();
	}

	/**
	 * The final discount value if any
	 */
	public function discount()
	{
		return $this->coupon ? $this->coupon->discount($this) : 0;
	}

	/**
	 * Assign a validated coupon to the order
	 */
	public function applyCoupon(Coupon $coupon)
	{
		$this->coupon = $coupon;
		return $this;
	}
}