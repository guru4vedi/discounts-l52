<?php

/**
 * Helpers
 */

use App\Currency;

/**
 * Get/Set User's Preferred Currency
 *
 * @today return values need to change
 */
function store_currency($prefCurrency="USD")
{
	return Currency::find($prefCurrency);
}