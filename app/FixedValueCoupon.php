<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Order;
use App\Currency;

class FixedValueCoupon extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Returns flat discount value in a given currency
     */
    private function valueInStoreCurrency(Currency $currency=null)
    {
        // If not provided, pick from context
        if(!$currency){
            $currency = store_currency();
        }

        return round(($this->value * $currency->conversion), 2);
    }

	/**
	 * Return the computed discount value
	 */
    public function discount(Order $order)
    {
    	// return (float) $this->value;
        return (float) $this->valueInStoreCurrency();
    }

    /**
     * Does it offer any price discount?
     */
    public function hasDiscount()
    {
        return true;
    }

    /**
     * Return the final value for a given amount
     */
    public function discountedPrice($price)
    {
        return  $price - $this->valueInStoreCurrency();
    }

    /**
     * Friendly Name of the coupon
     */
    public function screenName()
    {
        return sprintf("FixedValue %d Off", $this->value);
    }
}