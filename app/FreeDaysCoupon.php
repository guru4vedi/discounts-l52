<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Order;
use App\Currency;

class FreeDaysCoupon extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
